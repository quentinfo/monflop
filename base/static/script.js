/*------------------------
  ------ VARIABLES -------
  ------------------------*/

// input
var days = [{num: 0, ref: "m", name: "Lun."},
            {num: 1, ref: "tu", name: "Mar."},
            {num: 2, ref: "w", name: "Mer."},
            {num: 3, ref: "th", name: "Jeu."},
            {num: 4, ref: "f", name: "Ven."}] ;
var idays = {};
var groups = {} ;
var day_start = 0 ;      //8*60 ;
var day_end = 24*60 ;    //18*60 + 45 ;

const dayIndexes = new Map();
for(var day of days)
    dayIndexes.set(day.ref, day);

// data
var courses = [] ;
var modules = [];

// display settings
var min_to_px = 1 ;
var gp = {width: 30, nb_max: 7} ;


// initialisation function
function init() {
    
    d3.select("svg")
        .attr("height", svg_height())
        .attr("width", svg_width()) ;

    for(var i = 0 ; i<days.length ; i++){
        idays[days[i].ref] = days[i].num ;
    }

    groups["CE"] = {start: 0, width: 7} ;
    groups["1"] =  {start: 0, width: 2} ;
    groups["1A"] = {start: 0, width: 1} ;
    groups["1B"] = {start: 1, width: 1} ;
    groups["2"] =  {start: 2, width: 2} ;
    groups["2A"] = {start: 2, width: 1} ;
    groups["2B"] = {start: 3, width: 1} ;
    groups["3"] =  {start: 4, width: 2} ;
    groups["3A"] = {start: 4, width: 1} ;
    groups["3B"] = {start: 5, width: 1} ;
    groups["4"] =  {start: 6, width: 1} ;
    groups["234"]= {start: 2, width: 5} ;
}


/*------------------------
  ---- READ DATA FILE ----
  ------------------------*/

// Début de fonction pour récupérer les cours
// stockés sur le serveur 
function fetch_courses() {
    d3.csv(csvDataPath, function(data) {
        courses = data;
        displayCourses();
        displayGrid();
    });
}

function fetch_modules() {
    d3.csv(csvModulesPath, function(data) {
        modules = data;
        displayModules();
    });
}

function displayModules() {
    const table = document.getElementById("modules");
    for(var module of modules) {
        var row = document.createElement("tr");
        row.innerHTML = `<td>${module.name}</td>
                         <td>${module.abbrev}</td>
                         <td>${module.tutor_name}</td>`;
        table.appendChild(row);
    }
}

function displayGrid() {
    d3.select("svg#schedule")
    .selectAll(".course")
    .data(days)
    .enter()
    .append("rect")
    .attr("x", day_x)
    .attr("y", day_y)
    .attr("height", day_height)
    .attr("width", day_width)
    .attr("fill", "transparent")
    .attr("stroke", "black");
}

function displayCourses() {
    d3.select("svg#schedule")
    .selectAll(".course")
    .data(courses)
    .enter()
    .append("rect")
    .attr("x", course_x)
    .attr("y", course_y)
    .attr("height", course_height)
    .attr("width", course_width)
    .attr("fill", course => course.color_bg);

    d3.select("svg#schedule")
    .selectAll(".course")
    .data(courses)
    .enter()
    .append("text")
    .text(course => course.module)
    .attr("fill", course => course.color_txt)
    .attr("x", course => course_width(course)/2 + course_x(course))
    .attr("y", course => course_y(course)+20)
    .attr("text-align", "center")
    .attr("width", 100)
    .attr("font-size", 10)
    .attr("font-weight", "normal");

    d3.select("svg#schedule")
    .selectAll(".course")
    .data(courses)
    .enter()
    .append("text")
    .text(course => course.tutor_name)
    .attr("fill", course => course.color_txt)
    .attr("x", course => course_width(course)/2 + course_x(course))
    .attr("y", course => course_y(course)+40)
    .attr("font-size", 10)
    .attr("font-weight", "normal");

    d3.select("svg#schedule")
    .selectAll(".course")
    .data(courses)
    .enter()
    .append("text")
    .text(course => course.room)
    .attr("fill", course => course.color_txt)
    .attr("x", course => course_width(course)/2 + course_x(course))
    .attr("y", course => course_y(course)+60)
    .attr("font-size", 10)
    .attr("font-weight", "normal");
}

// Début de fonction pour transformer le CSV reçu
// en objet structuré js
function translate_courses_from_csv(d) {
    var co = {
        id_cours: +d.id_course,
        tutor: d.tutor_name,
        group: d.gp_name,
        module: d.module,
        c_type: d.coursetype,
    };
    return co ;
}

function day_width() {
    return gp.width * gp.nb_max * min_to_px;
}

function day_height() {
    return (90*7+15) * min_to_px;
}

function svg_height() {
    return (day_end - day_start) * min_to_px + 200 ;
}
function svg_width() {
    return days.length * gp.nb_max * gp.width;
}

function day_x(day) {
    return day.num * day_width();
}

function day_y(day) {
    return 0;
}

function course_day(course) {
    return dayIndexes.get(course.day);
}

function course_x(course) {
    return day_x(course_day(course)) + groups[course.gp_name].start * gp.width;
}

function course_y(course) {
    return day_y(course_day(course)) + (course.start_time-480) * min_to_px;
}

function course_height(course) {
    return course.duration;
}

function course_width(course) {
    return groups[course.gp_name].width * gp.width;
}

/*------------------
  ------ RUN -------
  ------------------*/

init();
fetch_courses() ;