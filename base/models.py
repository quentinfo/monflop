from django.db import models

# Create your models here.
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from enum import Enum
from people.models import Tutor


# <editor-fold desc="GROUPS">

# ------------
# -- GROUPS --
# ------------

class Group(models.Model):
    name = models.CharField(max_length=4)
    parent_group = models.ForeignKey('self',
                                     blank=True,
                                     null=True,
                                     related_name="children_group",
                                     on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def ancestor_groups(self):
        """
        :return: the set of all Group containing self (self not included)
        """
        all = set()

        if self.parent_group:
            all.add(self.parent_group)
            all |= self.parent_group.ancestor_groups()

        return all


# </editor-fold desc="GROUPS">

# <editor-fold desc="TIMING">
# ------------
# -- TIMING --
# ------------

# will be used only for constants
# TO BE CLEANED at the end (fields and ForeignKeys)
class Day(Enum):
    MONDAY = "m"
    TUESDAY = "tu"
    WEDNESDAY = "w"
    THURSDAY = "th"
    FRIDAY = "f"
    SATURDAY = "sa"
    SUNDAY = "su"


# </editor-fold>

# <editor-fold desc="ROOMS">
# -----------
# -- ROOMS --
# -----------


class RoomType(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=2)
    room_type = models.OneToOneField(RoomType,
                                     blank=True,
                                     null=True,
                                     related_name="members", on_delete=models.CASCADE)


# </editor-fold>

# <editor-fold desc="COURSES">
# -------------
# -- COURSES --
# -------------


class Module(models.Model):
    name = models.CharField(max_length=100, null=True)
    abbrev = models.CharField(max_length=10, verbose_name='Intitulé abbrégé')
    head = models.ForeignKey(Tutor,
                             null=True,
                             default=None,
                             blank=True,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.abbrev

    class Meta:
        ordering = ['abbrev', ]


class Course(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)
    tutor = models.ForeignKey(Tutor, null=True, on_delete=models.SET_NULL)
    module = models.ForeignKey(Module, null=True, on_delete=models.SET_NULL)
    room_type = models.ForeignKey(RoomType, null=True, on_delete=models.SET_NULL)
    week = models.PositiveIntegerField()
    year = models.PositiveSmallIntegerField()
    duration = models.PositiveSmallIntegerField()


# courses = Course.objects.all().filter(tutor=Tutor.objects.get(username="PSE"))


class ScheduledCourse(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    day = models.CharField(max_length=2, default=Day.MONDAY,
                           choices=[(d, d.value) for d in Day])
    # in minutes from 12AM
    start_time = models.PositiveSmallIntegerField()
    room = models.ForeignKey('room', blank=True, null=True, on_delete=models.CASCADE)


# </editor-fold desc="COURSES">


# <editor-fold desc="DISPLAY">
# -------------
# -- DISPLAY --
# -------------


class ModuleDisplay(models.Model):
    module = models.OneToOneField('Module', related_name='display', on_delete=models.CASCADE)
    color_bg = models.CharField(max_length=20, default="red")
    color_txt = models.CharField(max_length=20, default="black")

    def __str__(self):
        return f"{self.module} -> BG: {self.color_bg} ; TXT: {self.color_txt}"


class GroupDisplay(models.Model):
    group = models.OneToOneField('Group',
                                 related_name='display',
                                 on_delete=models.CASCADE)
    button_height = models.PositiveIntegerField(null=True, default=None)
    button_txt = models.CharField(max_length=30)

    def __str__(self):
        return f"{self.group} -> BH: {self.button_height} ; " + \
               f"BTXT: {self.button_txt}"
