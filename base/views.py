from django.shortcuts import render
from django.shortcuts import render

from django.http import HttpResponse
from django.contrib.staticfiles.views import serve
from base.admin import ScheduledCourseResource, ModuleResource

from base.models import Course, ScheduledCourse, Module


# Create your views here.


def hello_world(request):
    return HttpResponse("Salut !")


def display_year(request, year):
    return render(request,
                  'base/edt.html',
                  {'yyyy': year})


def display(request, year=2020, week=12):
    year = int(year)
    week = int(week)

    return render(request,
                  'edt.html',
                  {'yyyy': year,
                   'ww': week})


def fetch_courses(request, year, week):
    if year not in [2019, 2020]:
        year = 2019
    if week < 0 or week > 54:
        week = 0

    courses = Course.objects.filter(year=year, week=week)
    courses = ScheduledCourse.objects.filter(course__in=courses)

    return HttpResponse(ScheduledCourseResource().export(courses).csv, content_type="text/csv")


def fetch_modules(request):
    return HttpResponse(ModuleResource().export(Module.objects.all()).csv, content_type="text/csv")


def display_modules(request):
    return render(request, "modules.html", {})
