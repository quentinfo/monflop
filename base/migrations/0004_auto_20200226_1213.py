# Generated by Django 3.0.3 on 2020-02-26 11:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20200226_1212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='module',
            old_name='nom',
            new_name='name',
        ),
    ]
