# Generated by Django 3.0.3 on 2020-02-26 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_groupdisplay_button_txt'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='year',
            field=models.PositiveSmallIntegerField(default=2019),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='course',
            name='week',
            field=models.PositiveIntegerField(),
        ),
    ]
