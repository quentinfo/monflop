"""miniflop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path
from base import views

app_name = "base"

urlpatterns = [
    path('hello/', views.hello_world, name="hello"),
    re_path(r'^display/(?P<year>[0-9]{4})?(/(?P<week>\d{1,2}))?',
            views.display,
            name="display"),
    path('fetch_courses/<int:year>/<int:week>',
         views.fetch_courses,
         name="fetch_courses"),
    path('fetch_modules', views.fetch_modules, name="fetch_modules"),
    path('display_modules', views.display_modules, name="display_modules")
    # path("hello", views.hello, name="hello"),
    # path("display/", views.display, name="display"),
    # path("display/<int:year>", views.display, name="display"),
    # path("fetch_courses/<int:year>", views.fetch_courses, name="fetch_courses")
]
