from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Tutor(User):
    FULL_STAFF = "fs"
    SUPP_CHOICES = "ss"

    # TUTOR_CHOICES = {( FULL_STAFF, "Full staff"), (SUPP_STAFF, "Supply staff"))

    status = models.CharField(max_length=2,
                              # choices = TUTOR_CHOICES
                              )


class FullStaff(Tutor):
    class Meta:
        verbose_name = "Full Staff"


class SupplyStaff(Tutor):
    employer = models.CharField(max_length=50,
                                verbose_name="Employeur ?",
                                default=None, null=True, blank=True)  # Null pour la BD, Blank pour le formulaire
    position = models.CharField(max_length=50,
                                default=None, null=True, blank=True)  # Null pour la BD, Blank pour le formulaire
    field = models.CharField(max_length=50,
                             verbose_name="Domaine ?",
                             default=None, null=True, blank=True)  # Null pour la BD, Blank pour le formulaire

    class Meta:
        verbose_name = 'Supply Staff'


class Student(User):
    belong_to = models.ManyToManyField('base.group', blank=True)

    def __str__(self):
        return str(self.username) + '(G:' + str(self.belong_to) + ')'
