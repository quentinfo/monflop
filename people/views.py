from django.shortcuts import render

# Create your views here.

from people.admin import TutorResource
from django.http import HttpResponse
from people.models import Tutor
from django.template.response import TemplateResponse


def fetch_tutors(req):
    dataset = TutorResource().export(Tutor.objects.all())
    response = HttpResponse(dataset.csv, content_type="text/csv")
    return response


def tutors_email(req):
    return TemplateResponse(req, "index.html", {"tutors": Tutor.objects.all()})
