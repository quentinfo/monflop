from django.urls import path
from people import views

app_name = "people"

urlpatterns = [
    path('fetch_tutors', views.fetch_tutors, name="fetch_tutors"),
    path('mails', views.tutors_email, name="mails")
]
