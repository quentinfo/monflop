from django.contrib import admin

# Register your models here.
from people.models import Tutor
from import_export import resources, fields

class TutorAdmin(admin.ModelAdmin):
    list_display = ('username', 'status')
    ordering = ('username',)


admin.site.register(Tutor, TutorAdmin)


class TutorResource(resources.ModelResource):
    class Meta:
        model = Tutor
        fields = ("username", "first_name", "last_name", "email")
